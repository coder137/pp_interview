- [pp_interview](#pp_interview)
- [fcomp](#fcomp)
  - [compare_single branch](#compare_single-branch)
  - [compare_chunks branch](#compare_chunks-branch)

# pp_interview

# fcomp

The fcomp folder contains **Binary File comparison** code, readme and benchmark images

## compare_single branch

- Reads the binary file, a single **character** at a time
- Does not make use of multiread / multichunk performance
- Averages 45ms for a 1MB file

## compare_chunks branch

- Reads the file in **chunks** using `fread` API
  - Reading a block/chunk of data improves throughput
- Averages 3ms for a 1MB file
