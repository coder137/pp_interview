// Q2: Length of data in between sync bytes
// Length of data in between sync bytes
/*
The function should return the length of data which is between two y's.
Example: y=0xe, array = { 0x1,x2,0xe,0x2,0x3,0x4,0xe). Return 3.
Return -1 if the pattern is not found;
*/
#include <stdio.h>

int find(char *a, int len, char y) {
  int low = 0;
  int high = len;

  int low_index = -1;
  int high_index = -1;
  while (low < high) {
    if (a[low] == y) {
      low_index = low;
    }

    if (a[high] == y) {
      high_index = high;
    }

    if (low_index != -1 && high_index != -1) {
      break;
    }

    if (low_index == -1) {
      low++;
    }

    if (high_index == -1) {
      high--;
    }
  }

  return (high_index - low_index - 1);
}

int main() {
  printf("Start\r\n");
  char y = 0xe;
  char array[] = {0x1, 0x2, 0xe, 0x2, 0x3, 0x4, 0xe};

  int f = find(array, sizeof(array) / sizeof(char), y);
  printf("data: %d\r\n", f);

  return 0;
}
