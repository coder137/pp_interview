#include <stdint.h>
#include <stdio.h>

/*
 * Given a byte array(data) of length(len) search for the starting position
 * of 32 bit word 0xDEADBEEF and return the position
 * If it is not found return -1
 */
int search_sync(unsigned char *data, int len) {
  for (int i = 0; i < len; i++) {
    if (data[i] == 0xde) {

      // out of bounds check
      if (i + 3 >= len) {
        return -1;
      }

      // get the other 4 data here
      uint32_t fi = (data[i] << 24) | (data[i + 1] << 16) | (data[i + 2] << 8) |
                    (data[i + 3] << 0);
      if (fi == 0xdeadbeef) {
        return i;
      }
    }
  }

  return -1;
}

unsigned char d1[] = {0x12, 0x34, 0x56, 0x78, 0xde, 0xad, 0xbe, 0xef, 0, 0};
unsigned char d2[] = {0x12, 0x34, 0x56, 0x78, 0xde, 0xad, 0xbe, 0xe0, 0, 0};

int main(void) {
  int pos = -1;
  printf("In Main1\n");

  pos = search_sync(d1, 10);
  printf("pos=%d\n", pos);

  pos = search_sync(d1, 8);
  printf("pos=%d\n", pos);

  pos = search_sync(d2, 10);
  printf("pos=%d\n", pos);

  return 0;
}
