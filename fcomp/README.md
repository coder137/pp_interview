- [FCOMP](#fcomp)
- [Generate Project](#generate-project)
- [Build Project](#build-project)
- [Test Project](#test-project)
- [Benchmark for Compare via single read](#benchmark-for-compare-via-single-read)
- [[Improvement] Benchmark for Compare via chunk read](#improvement-benchmark-for-compare-via-chunk-read)

# FCOMP

Compare 2 binary files in C

# Generate Project

```
cmake -B build -G Ninja
```

# Build Project

```
cmake --build build
```

# Test Project

- Run `python create_random_binary.py`
  - This will create a 1MB file called `fr1.bin`
  - Copy the `fr1.bin` file and rename it to `fr2.bin`, open this in a hex editor or binary editor
  - Make your changes at the required bit locations
- Run the binary file created called `compare binary`
  - `compare_binary "fr1.bin" "fr2.bin"`

# Benchmark for Compare via single read

![Benchmark 1](image/Benchmark.png)

# [Improvement] Benchmark for Compare via chunk read

- Change `#define ARR_SZ 1000` to a value of your choosing
  - Keep this value under `4000`

![Benchmark 2](image/Benchmark_2.png)
