#include <error.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

// Defines
#define ARR_DIFF_BYTES 16
#define ARR_SZ 1000

// Constants
static const char FILE_MODE[] = "rb";

// Functions

// Hard exists the system under 3 circumstances
// 1. If arguments are lesser than 2
// 2. If F1 is NULL (not able to be opened)
// 3. If F2 is NULL (not able to be opened)
static void get_files(int argc, char **argv, FILE **f1, FILE **f2);

static size_t fill_array(FILE *f, uint8_t *arr, int sz);
static void print_array(const char *descriptor, FILE *f);

int fcomp_get_offset_read_chunks(FILE *f1, FILE *f2);

int main(int argc, char **argv) {
  FILE *f1, *f2;
  get_files(argc, argv, &f1, &f2);

  // Start the timer here
  struct timeval start, end;
  gettimeofday(&start, NULL);

  int offset = fcomp_get_offset_read_chunks(f1, f2);

  // Might throw error / not set seek position if wrong offset
  int rc1 = fseek(f1, offset, SEEK_SET);
  int rc2 = fseek(f2, offset, SEEK_SET);
  if (rc1 != 0 || rc2 != 0) {
    perror("fseek not set\r\n");
    exit(EXIT_FAILURE);
  }

  // End the timer here
  gettimeofday(&end, NULL);
  const double time_to_compare_s = (end.tv_sec - start.tv_sec) * 1000.0;
  const double time_to_compare_us = (end.tv_usec - start.tv_usec) / 1000.0;
  printf("*** Binary File Comparator ***\r\n");
  printf("Time_To_Compare: %f ms\r\n",
         (time_to_compare_s + time_to_compare_us));

  // Print information
  if (offset == -1) {
    // Success case
    // Both files have equal length
    // Both files are matching
  } else {
    // Completed running
    printf("Byte_Offset: %x\r\n", offset);
    printf("16_Bytes:\r\n");
    print_array("File1", f1);
    printf(" ");
    print_array("File2", f2);
  }

  // Close the two files
  fclose(f1);
  fclose(f2);

  return 0;
}

/**
 *
 * STATIC FUNCTIONS
 *
 */
static void get_files(int argc, char **argv, FILE **f1, FILE **f2) {
  // Check for 2 arguments
  if (argc <= 2) {
    printf("Requires 2 arguments to compare files\r\n");
    exit(ERROR_BAD_ARGUMENTS);
  }

  // Try opening file 1
  *f1 = fopen(argv[1], FILE_MODE);
  if (*f1 == NULL) {
    printf("File 1: %s does not exist\r\n", argv[1]);
    exit(ERROR_FILE_NOT_FOUND);
  }

  // Try opening file 2
  *f2 = fopen(argv[2], FILE_MODE);
  if (*f2 == NULL) {
    printf("File 2: %s does not exist\r\n", argv[2]);

    // Cleanup and exit
    fclose(*f1);
    exit(ERROR_FILE_NOT_FOUND);
  }
}

static size_t fill_array(FILE *f, uint8_t *arr, int sz) {
  size_t ret = fread((void *)arr, sizeof(uint8_t), sz, f);

  if (ret != sz && ferror(f)) {
    perror("Error reading file\r\n");
    exit(EXIT_FAILURE);
  }

  return ret;
}

static void print_array(const char *descriptor, FILE *f) {
  printf("%s:", descriptor);
  uint8_t arr[ARR_DIFF_BYTES] = {0};
  size_t sz = fill_array(f, arr, ARR_DIFF_BYTES);
  for (size_t i = 0; i < sz; i++) {
    printf("%x", arr[i]);
  }
}

/**
 * @brief Read binary file in CHUNKS and compare
 */

int fcomp_get_offset_read_chunks(FILE *f1, FILE *f2) {
  uint8_t arr1[ARR_SZ] = {0};
  uint8_t arr2[ARR_SZ] = {0};

  int offset = 0;
  bool different = false;
  bool empty = false;

  while (1) {
    // Fill the array
    fill_array(f1, arr1, ARR_SZ);
    fill_array(f2, arr2, ARR_SZ);

    // Iterate over the array and check for
    // 1. No matching binary values
    // 2. EOF values
    for (int i = 0; i < ARR_SZ; i++) {
      if (arr1[i] != arr2[i]) {
        different = true;
        break;
      }

      offset++;
    }

    if (feof(f1) && feof(f2)) {
      empty = true;
    }

    // If it is different or empty break here
    if (different || empty) {
      break;
    }
  }

  // If empty state is set, make the offset -1
  if (empty && !different) {
    offset = -1;
  }

  return offset;
}
