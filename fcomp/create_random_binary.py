import random 

f1 = open('fr1.bin', 'wb')

# Generate 1 MB random dataset
for i in range(0, 1024 * 1024):
  d = random.randint(0, 255)
  f1.write(d.to_bytes(1, byteorder='little'))
f1.close()
